<?php
    /**
     * Fixer.io API
     * 
     * @author      Goran Zivkovic
     * @version     1.0.0
     */
    Class Cf_GeoPlugin_Fixer_API
    {
        /**
         * Params for fixer.io API
         */
        protected $params = array(
            'base'          => '&base=EUR',
            'base_url'      => 'http://data.fixer.io/api/',
            'endpoint'      => 'latest',
            'access_key'    => '?access_key=8eb05634dac69a1b9efabe1d95fed93a'
        );

        /**
         * Database connection params.
         * 
         * @since       1.0.0
         */
        private $db = array(
            'servername'        => 'localhost',
            'username'          => 'root',
            'password'          => '',
            'db_name'            => 'cf_fixer_api',
            'table_name'        => 'cf_geoplugin_fixer_api',
            'server_port'       => 3306
        );

        /**
         * Initialize class.
         * 
         * @since       1.0.0
         */
        public function __construct()
        {
            $this->cf_geoplugin_fixer_create_table();
            $this->cf_geoplugin_fixer_get_all_currency();
        }

        /**
         * Create or update table in database.
         * 
         * @since       1.0.0
         */
        public function cf_geoplugin_fixer_create_table()
        {
            $mysqli = $this->cf_geoplugin_fixer_create_connection();

            $table = $this->db['table_name'];
            // sql to create table
            $sql = "
                CREATE TABLE IF NOT EXISTS $table (
                currency_slug varchar(3)  PRIMARY KEY, 
                currency_value float(30) NOT NULL,
                currency_timespamp int(10) NOT NULL );
            ";

            $mysqli->query( $sql );

            $mysqli->close();
        }

        /**
         * API call for getting latest informations about currencies.
         * 
         * @since       1.0.0
         */
        public function cf_geoplugin_fixer_get_all_currency()
        {
            $rates = array();

            $curl_url = $this->params['base_url'] . $this->params['endpoint'] . $this->params['access_key'] . $this->params['base'];
            $ch = curl_init( $curl_url );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $json = curl_exec( $ch );
            curl_close( $ch );

            $rates = json_decode( $json, true );
            $timestamp = $rates['timestamp'];
            $rates = $rates['rates'];

            if( ! empty( $rates ) )
            {
                $this->cf_geoplugin_fixer_insert_currencies( $rates, $timestamp );
            }
        }

        /**
         * Insert informations into db.
         * 
         * @since       1.0.0
         * @param       array       $rates      List of latest rates of currencies recived by API.
         * @param       int         $timestamp  Timestamp of API call.
         */
        public function cf_geoplugin_fixer_insert_currencies( $rates, $timestamp )
        {

           $mysqli = $this->cf_geoplugin_fixer_create_connection();

           $currency_slugs = $this->cf_geoplugin_fixer_get_all_currency_slug( $mysqli );

           $table = $this->db['table_name'];
           foreach( $rates as $key => $value )
           {
               $sql = "
                    INSERT INTO $table ( currency_slug, currency_value, currency_timespamp )
                    VALUES ( '$key', $value, $timestamp );
               ";
               if( in_array( $key, $currency_slugs ) )
               {
                   $sql = "
                        UPDATE $table
                        SET currency_value=$value, currency_timespamp=$timestamp
                        WHERE currency_slug='$key';
                   ";
               }
               $mysqli->query( $sql );
           }

           $mysqli->close();
        }

        /**
         * Get all currency_slug from db.
         * 
         * @since       1.0.0
         * @param       object      $mysqli     DB object.
         * @return      array       $currency_slugs     Array of all currency_slug from DB.
         */
        public function cf_geoplugin_fixer_get_all_currency_slug( $mysqli )
        {
            $currency_slugs = array();

            $table = $this->db['table_name'];
            $sql = "
                SELECT currency_slug
                FROM $table;
            ";

            $results = $mysqli->query( $sql, MYSQLI_USE_RESULT );
            
            while( $row = $results->fetch_array() )
            {
                $currency_slugs[] = $row[0];
            }

            return $currency_slugs;
        }

        /**
         * Convert rates algorithm.
         * 
         * @since       1.0.0
         * @param       string      $base_currency_slug     Currency slug that you want to be base of convertion.
         * @return      array       $currencies_rates       Array of currencies rates by base currency.
         */
        public function cf_geoplugin_fixer_convert_currencies( $base_currency_slug = 'EUR', $decimal_round = 13 )
        {
            $currencies_rates = $this->cf_geoplugin_fixer_get_all_currency_info();

            if( empty( $currencies_rates ) )
            {
                return false;
            }

            if( $base_currency_slug == 'EUR' )
            {
                return $currencies_rates;
            }

            if( ! array_key_exists( $base_currency_slug, $currencies_rates ) )
            {
                return false;
            }

            $base_currency_value = $currencies_rates[ $base_currency_slug ];
            
            foreach( $currencies_rates as $key => $value )
            {
                $currencies_rates[ $key ] = round( $value / $base_currency_value, $decimal_round, PHP_ROUND_HALF_UP );
            }
                
            var_dump( $currencies_rates );
            return $currencies_rates;

        }

        /**
         * Get all informations about currencies in db.
         * 
         * @since       1.0.0
         * @return      array       $currencies_rates       Currencies rates recived from DB.
         */
        public function cf_geoplugin_fixer_get_all_currency_info()
        {
            $currencies_rates = array();

            $mysqli = $this->cf_geoplugin_fixer_create_connection();

            $table = $this->db['table_name'];
            $sql = "
                SELECT currency_slug, currency_value
                FROM $table;
            ";

            $results = $mysqli->query( $sql, MYSQLI_USE_RESULT );
            
            while( $row = $results->fetch_array() )
            {
                $currencies_rates[ $row[0] ] = $row[1];
            }

            $mysqli->close();

            return $currencies_rates;
        }

        /**
         * Create connection to server.
         * 
         * @since       1.0.0
         * @return      object      $mysqli     New connection to server.
         */
        public function cf_geoplugin_fixer_create_connection()
        {
            $mysqli = new mysqli( $this->db['servername'], $this->db['username'], $this->db['password'], $this->db['db_name'], $this->db['server_port']);
            if ($mysqli->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            return $mysqli;
        }
        
    }
    $cf_geoplugin_fixer_api = new Cf_GeoPlugin_Fixer_API();
    $cf_geoplugin_fixer_api->cf_geoplugin_fixer_convert_currencies('USD');
?>